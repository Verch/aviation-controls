#include <Arduino.h>
#include <Adafruit_MCP23X17.h>
#include <ArduinoSTL.h>
#include <mcp.h>
#include <mcp_test.h>
// #include <mcp_avionics_1.h>
// #include <mcp_avionics_2.h>
// #include <mcp_swtichrow_lights.h>
// #include <mcp_swtichrow_engines.h>
#include <Joystick.h>

/* !!! Arduino Leonardo Pin 5 on PULLUP disrupts I2C !!!*/
const uint8_t ARD_PIN_TO_MCP1 = 4;
const uint8_t ARD_PIN_TO_MCP2 = 9;
const uint8_t ARD_PIN_TO_SW_LT = 10; 
const uint8_t ARD_PIN_TO_SW_ENG = 13;
const uint8_t ARD_PIN_TO_TEST = 14;

void configure_mcp(McpInstruments* McpIn) {
  /*
   * Configure MCP Pins
   * Buttons will be debounced manually, so we check on LOW,
   * Encoder signals will trigger on CHANGE 
   */
  Serial.println("Configuring pins...");
  for(int i=0; i<16; i++){
    // Skip Pins that interfere with I2C
    if(i == MCP_GPA7 || i == MCP_GPB7){continue;}
    // Skip undefined
    if(McpIn->instruments[i] == 0){continue;}
    if(McpIn->instruments[i]->isType() == InstrumentType::undefined){continue;}
    McpIn->MCP->pinMode(i, INPUT_PULLUP); 
    if(McpIn->instruments[i]->isType() == InstrumentType::button){McpIn->MCP->setupInterruptPin(i, CHANGE);Serial.print("Button congigured: ");Serial.println(i);}
    else if(McpIn->instruments[i]->isType() == InstrumentType::encoder){McpIn->MCP->setupInterruptPin(i, CHANGE);Serial.print("Encoder congigured: ");Serial.println(i);}
    else if(McpIn->instruments[i]->isType() == InstrumentType::ioswitch){McpIn->MCP->setupInterruptPin(i, CHANGE);Serial.print("Switch congigured: ");Serial.println(i);}
    else {Serial.print("Error, instrument type unknown: ");Serial.print(i);
      Serial.println(McpIn->instruments[i]->isType());}
  }

  /* mirror INTA/B so only one wire required */
  McpIn->MCP->setupInterrupts(true, false, LOW);
  
  /* Better safe than sorry: 
   * Disable pins that are disrupting I2C */
  McpIn->MCP->disableInterruptPin(MCP_GPA7); McpIn->MCP->pinMode(MCP_GPA7, OUTPUT);
  McpIn->MCP->disableInterruptPin(MCP_GPB7); McpIn->MCP->pinMode(MCP_GPB7, OUTPUT);
}

InstrumentAction readActionFromMcp(
  uint8_t arduino_pin, 
  McpInstruments* McpIn){
  // DigitalRead is negated, read would be !digitalRead()
  if (digitalRead(arduino_pin)) { return InstrumentAction::nothing; }
  uint8_t pin = McpIn->MCP->getLastInterruptPin();
  McpIn->MCP->clearInterrupts();
  
  return McpIn->getActionForPin(pin);
}

uint8_t last_triggered_pin;

/*** MAIN ***/

void setup() {
  /* Configure Arduino Serial communication */
  while(!Serial){
    Serial.begin(9600); // debug
    delay(200);
  }
  Serial.println("Serial communication started!"); // debug

  /* Configure Arduino Pins */
  // pinMode(ARD_PIN_TO_MCP1, INPUT_PULLUP);
  // pinMode(ARD_PIN_TO_MCP2, INPUT_PULLUP);
  // pinMode(ARD_PIN_TO_SW_LT, INPUT_PULLUP);
  // pinMode(ARD_PIN_TO_SW_ENG, INPUT_PULLUP);
  pinMode(ARD_PIN_TO_TEST, INPUT_PULLUP);

  /* Configure I2C Bus */
  Serial.println("Talking to MCPs..."); // debug
  // if(!MCP_Avionics_1.begin_I2C(MCP_AVC_1_ADDRESS)){Serial.println("Error communicating to MCP_AVC_1");}
  // else{Serial.println("MCP_AVC_1 available");} // debug
  // if(!MCP_Avionics_2.begin_I2C(MCP_AVC_2_ADDRESS)){Serial.println("Error communicating to MCP_AVC_2");}
  // else{Serial.println("MCP_AVC_2 available");} // debug
  // if(!MCP_Switches_Lights.begin_I2C(MCP_SW_LT_ADDRESS)){Serial.println("Error communicating to MCP_SW_LT");}
  // else{Serial.println("MCP_SW_LT available");} // debug
  // if(!MCP_Switches_Lights.begin_I2C(MCP_SW_ENG_ADDRESS)){Serial.println("Error communicating to MCP_SW_ENG");}
  // else{Serial.println("MCP_SW_ENG available");} // debug
  if(!MCP_Test.begin_I2C(MCP_TEST_ADDRESS)){Serial.println("Error communicating to MCP_TEST");}
  else{Serial.println("MCP_TEST available");} // debug

  /* Configure MCPs */
//  configure_mcp(MCP_Avionics_1, mcp_avc_1_instruments);
//  configure_mcp(MCP_Avionics_2, mcp_avc_2_instruments);
//  configure_mcp(MCP_Switches_Lights, mcp_sw_light_instruments);
//  configure_mcp(MCP_Switches_Engines, mcp_sw_eng_instruments);
 configure_mcp(&McpTest);

  Serial.println("Looping..."); // debug
  //TODO read once
}

void loop() {
  /* MCP TEST */
  InstrumentAction action = InstrumentAction::nothing;
  action = readActionFromMcp(
    ARD_PIN_TO_TEST,
    &McpTest);
  
  if      (action == InstrumentAction::button_pushed)     { Serial.println("Button pressed"); }
  else if (action == InstrumentAction::switch_off)        { Serial.println("Switch off"); }
  else if (action == InstrumentAction::switch_on)         { Serial.println("Switch on"); }
  else if (action == InstrumentAction::encoder_increase)  { Serial.println("Encoder increase"); }
  else if (action == InstrumentAction::encoder_decrease)  { Serial.println("Encoder descrease"); }
  else if (action == InstrumentAction::error)             { Serial.println("Instrument error"); }
  


  // /* MCP1 */  
  // if (!digitalRead(ARD_PIN_TO_MCP1)) {
  //   // Get trigger
  //   last_triggered_pin = MCP_Avionics_1.getLastInterruptPin();
  //   mcp_avc_1_instruments[last_triggered_pin]->trigger();
  //   MCP_Avionics_1.clearInterrupts();
  
  //   // Check for valid activation
  //   if(mcp_avc_1_instruments[last_triggered_pin]->wasActivated()){
  //     Serial.print("MCP1: ");
  //     if(mcp_avc_1_instruments[last_triggered_pin]->isType() == InstrumentType::button){
  //       // Button
  //       Serial.print("Button activated: "); // debug
  //       Serial.println(last_triggered_pin); // debug
  //       }
  //     else if (mcp_avc_1_instruments[last_triggered_pin]->isType() == InstrumentType::encoder){
  //       // Encoder
  //       Serial.print("Encoder activated: "); // debug
  //       Serial.println(last_triggered_pin); // debug
  //       // Debounce second encoder input
  //       reverse_map_mcp_avc_1_encoders[last_triggered_pin]->bounceTwice();
  //       }
  //     }
  //     mcp_avc_1_instruments[last_triggered_pin]->resetTrigger();
  //   }

  
  // /* MCP2 */
  // if (!digitalRead(ARD_PIN_TO_MCP2)) {
  //   // Get trigger
  //   last_triggered_pin = MCP_Avionics_2.getLastInterruptPin();
  //   mcp_avc_2_instruments[last_triggered_pin]->trigger();
  //   MCP_Avionics_2.clearInterrupts();

  //   // Check for valid activation
  //   if(mcp_avc_2_instruments[last_triggered_pin]->wasActivated()){
  //     Serial.print("MCP2: ");
  //     if(mcp_avc_2_instruments[last_triggered_pin]->isType() == InstrumentType::button){
  //       // Button
  //       Serial.print("Button activated: "); // debug
  //       Serial.println(last_triggered_pin); // debug
  //       }
  //     else if (mcp_avc_2_instruments[last_triggered_pin]->isType() == InstrumentType::ioswitch){
  //       // Encoder
  //       Serial.print("Switch activated: "); // debug
  //       Serial.println(last_triggered_pin); // debug
  //       }
  //     else if (mcp_avc_2_instruments[last_triggered_pin]->isType() == InstrumentType::encoder){
  //       // Encoder
  //       Serial.print("Encoder activated: "); // debug
  //       Serial.println(last_triggered_pin); // debug
  //       // Debounce second encoder input
  //       reverse_map_mcp_avc_2_encoders[last_triggered_pin]->bounceTwice();
  //       }
  //   }
  //   mcp_avc_2_instruments[last_triggered_pin]->resetTrigger();
  // }

  // /* MCP3 */
  // if (!digitalRead(ARD_PIN_TO_SW_LT)) {
  //   // Get trigger
  //   last_triggered_pin = MCP_Switches_Lights.getLastInterruptPin();
  //   mcp_sw_light_instruments[last_triggered_pin]->trigger();
  //   MCP_Switches_Lights.clearInterrupts();

  //   // Check for valid activation
  //   if(mcp_sw_light_instruments[last_triggered_pin]->wasActivated()){
  //     Serial.print("MCP3: ");
  //     if(mcp_sw_light_instruments[last_triggered_pin]->isType() == InstrumentType::ioswitch){
  //       // Button
  //       Serial.print("Switch activated: "); // debug
  //       Serial.println(last_triggered_pin); // debug
  //       }
  //   }
  //   mcp_sw_light_instruments[last_triggered_pin]->resetTrigger();
  // }

  // /* MCP4 */
  // if (!digitalRead(ARD_PIN_TO_SW_ENG)) {
  //   // Get trigger
  //   last_triggered_pin = MCP_Switches_Engines.getLastInterruptPin();
  //   mcp_sw_eng_instruments[last_triggered_pin]->trigger();
  //   MCP_Switches_Engines.clearInterrupts();
  //   Serial.print("Pin triggered: ");
  //   Serial.println(last_triggered_pin);

  //   // Check for valid activation
  //   if(mcp_sw_eng_instruments[last_triggered_pin]->wasActivated()){
  //     Serial.print("MCP4: ");
  //     if(mcp_sw_eng_instruments[last_triggered_pin]->isType() == InstrumentType::ioswitch){
  //       // Button
  //       Serial.print("Switch activated: "); // debug
  //       Serial.println(last_triggered_pin); // debug
  //       }
  //     Serial.println("activated");
  //   }
  //   mcp_sw_eng_instruments[last_triggered_pin]->resetTrigger();
  // }

  Serial.println("Still looping");
 }
