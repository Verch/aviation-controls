#include <Arduino.h>
#include <Adafruit_MCP23X17.h>
#include <ArduinoSTL.h>
#include <mcp.h>
#include <adresses.h>
#include <Joystick.h>

#ifndef mcp_test
#define mcp_test

Joystick_ JoystickMcpTest(MCP_TEST_JOYSTICK_ID, 
  JOYSTICK_TYPE_MULTI_AXIS, 4, 0,
  false, false, false, false, false, false,
  false, false, false, false, false);

// Set to true to test "Auto Send" mode or false to test "Manual Send" mode.
//const bool testAutoSendMode = true;
const bool testAutoSendMode = false;

const unsigned long gcCycleDelta = 1000;
const unsigned long gcAnalogDelta = 25;
const unsigned long gcButtonDelta = 500;
unsigned long gNextTime = 0;
unsigned int gCurrentStep = 0;

Adafruit_MCP23X17 MCP_Test;

Encoder Enc1(1,2);
Button But1;
Switch Swt1;

std::vector<Instrument*> instruments_test {
&Enc1,  //  MCP_GPA0
&Enc1,  //  MCP_GPA1
&But1,  //  MCP_GPA2
&Swt1,  //  MCP_GPA3
0,  //  MCP_GPA4
0,  //  MCP_GPA5
0,  //  MCP_GPA6
0,      //  MCP_GPA7
0,  //  MCP_GPB0
0,  //  MCP_GPB1
0,  //  MCP_GPB2
0,      //  MCP_GPB3
0,      //  MCP_GPB4
0,  //  MCP_GPB5
0,      //  MCP_GPB6
0       //  MCP_GPB7
};

McpInstruments McpTest(
    MCP_TEST_ADDRESS,
    &instruments_test, 
    &MCP_Test);

#endif