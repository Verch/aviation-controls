#include <Arduino.h>
#include <Adafruit_MCP23X17.h>
#include <ArduinoSTL.h>
#include <mcp.h>

#ifndef mcp_sw_eng
#define mcp_sw_eng

#define MCP_SW_ENG_ADDRESS 0x26
Adafruit_MCP23X17 MCP_Switches_Engines;

Switch SE_Sw2,SE_Sw3,SE_Sw4,SE_Sw5,SE_Sw6,SE_Sw7,SE_Sw8,SE_Sw9,SE_Sw10,SE_Sw11,SE_Sw12,SE_Sw13;
Empty None;

std::vector<Instrument*> mcp_sw_eng_instruments {
0, //&SE_Sw1,     //  MCP_GPA0
&SE_Sw2,     //  MCP_GPA1
&SE_Sw3,     //  MCP_GPA2
&SE_Sw4,     //  MCP_GPA3
&SE_Sw5,     //  MCP_GPA4
&SE_Sw6,     //  MCP_GPA5
&SE_Sw7,     //  MCP_GPA6
0,              //  MCP_GPA7
&None,
&None,
&None,
&None,
&None,
&None,
// &SE_Sw8,     //  MCP_GPB0
// &SE_Sw9,     //  MCP_GPB1
// &SE_Sw10,     //  MCP_GPB2
// &SE_Sw11,     //  MCP_GPB3
// &SE_Sw12,     //  MCP_GPB4 
// &SE_Sw13,     //  MCP_GPB5
&None,              //  MCP_GPB6
0              //  MCP_GPB7
};

// Switches only

#endif