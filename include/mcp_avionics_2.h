#include <Arduino.h>
#include <Adafruit_MCP23X17.h>
#include <ArduinoSTL.h>
#include <mcp.h>

#ifndef mcp_avc_2
#define mcp_avc_2

#define MCP_AVC_2_ADDRESS 0x24
Adafruit_MCP23X17 MCP_Avionics_2;

Button AVC2_But1,AVC2_But2,AVC2_But3,AVC2_But4,AVC2_But5,AVC2_But6,AVC2_But7,AVC2_But8,AVC2_But9,AVC2_But10;
Switch AVC2_Swt1;
Encoder ACV2_Enc1, ACV2_Enc2;

std::vector<Instrument*> mcp_avc_2_instruments {
&ACV2_Enc1,     //  MCP_GPA0
&ACV2_Enc2,     //  MCP_GPA1
&AVC2_But3,     //  MCP_GPA2
&AVC2_But4,     //  MCP_GPA3
&AVC2_But5,     //  MCP_GPA4
&AVC2_But6,     //  MCP_GPA5
&AVC2_But7,     //  MCP_GPA6
0,              //  MCP_GPA7
&AVC2_Swt1,     //  MCP_GPB0
&AVC2_But8,     //  MCP_GPB1
&AVC2_But9,     //  MCP_GPB2
&AVC2_But1,     //  MCP_GPB3
&AVC2_But2,     //  MCP_GPB4 
&AVC2_But10,     //  MCP_GPB5
0,              //  MCP_GPB6
0,              //  MCP_GPB7
};

std::vector<Instrument*> reverse_map_mcp_avc_2_encoders {
&ACV2_Enc2,
&ACV2_Enc1,
0,
0,
0,
0,
0,
0,
0,
0,
0,
0,
0,
0,     
0,     
0
};

#endif