#include <Arduino.h>
#include <Adafruit_MCP23X17.h>
#include <ArduinoSTL.h>
#include <mcp.h>

#ifndef mcp_sw_lt
#define mcp_sw_lt

#define MCP_SW_LT_ADDRESS 0x27
Adafruit_MCP23X17 MCP_Switches_Lights;

Switch SL_Sw2,SL_Sw3,SL_Sw4,SL_Sw5,SL_Sw6,SL_Sw7,SL_Sw8,SL_Sw9,SL_Sw10,SL_Sw11;

std::vector<Instrument*> mcp_sw_light_instruments {
0,     //  MCP_GPA0
&SL_Sw11,     //  MCP_GPA1
&SL_Sw3,     //  MCP_GPA2
&SL_Sw4,     //  MCP_GPA3
&SL_Sw5,     //  MCP_GPA4
&SL_Sw6,     //  MCP_GPA5
&SL_Sw7,     //  MCP_GPA6
0,              //  MCP_GPA7
&SL_Sw8,     //  MCP_GPB0
&SL_Sw9,     //  MCP_GPB1
&SL_Sw10,     //  MCP_GPB2
0,     //  MCP_GPB3
0,              //  MCP_GPB4 
&SL_Sw2,              //  MCP_GPB5
0,              //  MCP_GPB6
0,              //  MCP_GPB7
};

// Switches only

#endif