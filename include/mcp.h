#include <Arduino.h>

#ifndef mcp_h
#define mcp_h

#define MCP_GPA0 0
#define MCP_GPA1 1
#define MCP_GPA2 2
#define MCP_GPA3 3
#define MCP_GPA4 4
#define MCP_GPA5 5
#define MCP_GPA6 6
#define MCP_GPA7 7 // ONLY OUT!
#define MCP_GPB0 8
#define MCP_GPB1 9
#define MCP_GPB2 10
#define MCP_GPB3 11
#define MCP_GPB4 12
#define MCP_GPB5 13
#define MCP_GPB6 14
#define MCP_GPB7 15 // ONLY OUT!

/*
BUTTON
Adapted to activate, when interrupt on LOW is triggered (default pullup).
1)Activate only once when state changes, and not when state changes back to HIGH.
2)Avoid second activation by fast state changes via debouncing

￣￣￣\   _                       /￣￣￣￣￣￣￣￣
        \/  \/\__________________/
        o---------|


SWICH
Trigger on every CHANGE (default pullup).

￣￣￣\   _                       
        \/  \/\__________________
        o---------|
        

ENCODER
Adapted to activate, when interrupt on CHANGE is triggered.
1)Avoid second activation by fast state changes via debouncing
2)Avoid antagonizing activation by complimentary Encoder via double debounce time

E1  ￣￣￣\   _       
            \/  \/\____________________

E2  ￣￣￣￣￣￣\  _       
                \/  \/\______________

            o-------|-------|

*/

enum InstrumentType{
    undefined,
    button,
    encoder,
    ioswitch
};

enum InstrumentAction{
    error,
    nothing,
    button_released,
    button_pushed,
    switch_off,
    switch_on,
    encoder_decrease,
    encoder_increase
};

class Instrument{
    protected:
        unsigned long debounce_time = 150;
        unsigned long last_activation = 0;
        bool old_state = false;
        bool new_state = false;
    public:
        virtual InstrumentType isType(){return InstrumentType::undefined;}
        virtual bool wasActivated() = 0;
        InstrumentAction getAction(uint8_t pin){ return InstrumentAction::error; };
        
        void trigger(){new_state = true;}
        void saveState(){old_state = new_state;}

        // For debouncing: Use to avoid second activation withing debounce time
        void bounce(){last_activation = millis();}        
        bool isBouncing(){
            if(last_activation + debounce_time > millis()){return true;}
            else{return false;}
        }
        void bounceTwice(){last_activation = millis() + debounce_time;} // Double debounce time for complimentary instruments
        // Reset internal states
        void resetTrigger(){new_state = false;}
        void resetState(){old_state = false;}
        // Debugging
        unsigned long getLastActivation(){return last_activation;}
        bool getState(){return old_state;}
        bool getTrigger(){return new_state;}
};

class Empty: public Instrument{
    InstrumentType isType(){return InstrumentType::undefined;}
    bool wasActivated(){return false;}
};

class Button: public Instrument{
    private:
        unsigned long debounce_time = 1000;
    public:
        InstrumentType isType(){return InstrumentType::button;};
        bool wasActivated(){
                if(old_state == false && new_state == true && !isBouncing()){
                        bounce();
                        saveState();
                        return true;
                    }
                return false;
            }
};

class Switch: public Instrument{
    public:
        InstrumentType isType(){return InstrumentType::ioswitch;};
        bool wasActivated(){
                if(!isBouncing()){
                        // TODO READ STATE
                        bounce();
                        return true;
                    }
                return false;
        }
        bool isBouncing(){return true;}
};

class EncoderPin{
    public:
        bool pin_state = 0; 
        void triggger(){ pin_state = !pin_state; }
};

class Encoder: public Instrument{
    public:
        int8_t internal_state = 0;
        EncoderPin* Pin_1;
        EncoderPin* Pin_2;
        uint8_t address_pin_1, address_pin_2;

        Encoder(uint8_t address_pin_1, uint8_t address_pin_2){
            EncoderPin Pin1, Pin2;
            Pin_1 = &Pin1;
            Pin_2 = &Pin2;
            address_pin_1 = address_pin_1;
            address_pin_2 = address_pin_2;
        }

        InstrumentType isType(){return InstrumentType::encoder;};
        bool wasActivated(){ return 1; };
        InstrumentAction getAction(uint8_t pin){
            bool direction;
            if      (pin == address_pin_1){Pin_1->triggger(); direction=LOW; }
            else if (pin == address_pin_2){Pin_2->triggger(); direction=HIGH; }
            else { return InstrumentAction::nothing; }
            // Evaluate#
            if (Pin_1->pin_state == HIGH && Pin_2->pin_state == HIGH){
                Pin_1->pin_state = 0;
                Pin_2->pin_state = 0;
                if      ( direction == LOW  ) { return InstrumentAction::encoder_decrease; }
                else if ( direction == HIGH ) { return InstrumentAction::encoder_increase; }
            }
            return InstrumentAction::nothing;
        }
};


class McpInstruments{
    public:
        uint8_t i2c_address;
        std::vector<Instrument*> instruments;
        Adafruit_MCP23X17* MCP;

        McpInstruments(
            uint8_t i2c_address,
            std::vector<Instrument*>* instruments, 
            Adafruit_MCP23X17* MCP){ 
                i2c_address = i2c_address;
                instruments = instruments; 
                MCP = MCP;
        };

        InstrumentAction getActionForPin(uint8_t pin){
            Instrument* instr = instruments[pin];
            if(instr->isType() == InstrumentType::button){
                if(instr->isBouncing()){ return InstrumentAction::nothing; }
                instr->bounce();
                if(MCP->digitalRead(pin) == LOW){ return InstrumentAction::button_pushed; } 
                else { return InstrumentAction::button_released; }
            }
            else if (instr->isType() == InstrumentType::ioswitch){
                if(instr->isBouncing()){ return InstrumentAction::nothing; }
                instr->bounce(); 
                if(MCP->digitalRead(pin) == LOW){ return InstrumentAction::switch_on; } 
                else { return InstrumentAction::switch_off; }
            }
            else if (instr->isType() == InstrumentType::encoder){
                if(instr->wasActivated()){
                    InstrumentAction action = instr->getAction(pin);
                    return action;
                }
            }
            return InstrumentAction::error;
        }
};

#endif