#include <Arduino.h>
#include <Adafruit_MCP23X17.h>
#include <ArduinoSTL.h>
#include <mcp.h>

#ifndef mcp_avc_1
#define mcp_avc_1

#define MCP_AVC_1_ADDRESS 0x20
Adafruit_MCP23X17 MCP_Avionics_1;

Encoder Enc1,Enc2,Enc3,Enc4,Enc5,Enc6,Enc7,Enc8;
Button But1,But2,But3;

std::vector<Instrument*> mcp_avc_1_instruments {
&Enc1,  //  MCP_GPA0
&Enc2,  //  MCP_GPA1
&But1,  //  MCP_GPA2
&Enc3,  //  MCP_GPA3
&Enc4,  //  MCP_GPA4
&Enc5,  //  MCP_GPA5
&Enc6,  //  MCP_GPA6
0,      //  MCP_GPA7
&Enc7,  //  MCP_GPB0
&Enc8,  //  MCP_GPB1
&But2,  //  MCP_GPB2
0,      //  MCP_GPB3
0,      //  MCP_GPB4
&But3,  //  MCP_GPB5
0,      //  MCP_GPB6
0       //  MCP_GPB7
};

std::vector<Instrument*> reverse_map_mcp_avc_1_encoders {
&Enc2,  //  E1
&Enc1,  //  E2
0,
&Enc4,  //  E3
&Enc3,  //  E4
&Enc6,  //  E5
&Enc5,  //  E6
0,
&Enc8,  //  E7
&Enc7,  //  E8
0,
0,
0,
0,
0,
0
};

#endif